# Angular homework #2

## Requirements
We are building an Advertising system this time, user should be able to advertise products (something like ss.lv).
Create an angular application that works with provided backend. Angular application should contain:
1. Main window where a user selects category which he wants to browse.
2. Category browser- page where user can browse all advertisements in the specified directory. 
3. Advertisement page- page where user can see all the info about advert (this should be opened when user clicks an advert in 'Category browser' page).
4. Search page- user should be able to search for adverts by title and/or description. By clicking on a search result user is navigated to Advertisement page.
5. Advertisement editor (CRUD operations)
6. User editor (CRUD operations)
7. Category editor (CRUD operations)
8. Bonus points:
    - maket it with pretty design
    - reactive form usage
    - using Bootstrap and/or Angular Material
    - creating your own backend with Spring Boot (you can change the database structure if you go this way)
    - using MySQL instead of H2 (draw ER model, add database scripts)
    - additional filtering options on Category browser (i.e. filter all adverts within specific price range)
    - add sub-categories (i.e.: we have category 'Cars' and below it there should be categories 'BMW', 'Audi', etc...)

## How to run backend

1. Find application file:
```
backend/src/main/java/com/sda/angularhomeworkbackend/AngularHomeworkAdvertServiceApplication.java
```
2. Right click and 'Debug AngularHomeworkAdvertServiceApplication'

This will run the backend and run the in-memory database with all the data that is required for our application. 
But remember that database that is used is in-memory and all the changes will be lost when you restart your backend application. 
This was to done to help you with testing you angular application.

## How to see what is in database
1. Go to url(http://localhost:8080/h2). You will see a database console window
2. In field *JDBC URL* type in ```jdbc:h2:mem:test```
3. Press connect

In the console you can browse tables, see the structure, and see the data that is in our running database.

## Backend endpoints
Backend should be called by address ```localhost:8080```. You can check what specific endpoints are availabe in Controller classes of Java application.

## Postman collection
No postman collection this time, investigate backend code yourself! If you will get stuck without it, ask me in Slack- I will send you the collection :)




