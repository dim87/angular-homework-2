package com.sda.angularhomeworkbackend.modules.adverts;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "adverts")
class Advert {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	@Column
	private String title;

	@Column
	private String description;

	@Column
	private BigDecimal price;

	@Column
	private Long userId;

	@Column
	private Long categoryId;

	@Column
	private Date published;

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(final BigDecimal price) {
		this.price = price;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(final Long userId) {
		this.userId = userId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(final Long categoryId) {
		this.categoryId = categoryId;
	}

	public Date getPublished() {
		return published;
	}

	public void setPublished(final Date published) {
		this.published = published;
	}

	@Override
	public String toString() {
		return "Advert{" + "id=" + id + ", title='" + title + '\'' + ", description='" + description + '\'' + ", price=" + price + ", userId=" + userId +
			   ", categoryId=" + categoryId + ", published=" + published + '}';
	}
}
