package com.sda.angularhomeworkbackend.modules.adverts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/adverts")
public class AdvertController {

	@Autowired
	private AdvertService advertService;

	@GetMapping
	public List<Advert> getAll() {
		return advertService.findAll();
	}

	@GetMapping(value = "/{advertId}")
	public Advert getById(@PathVariable long advertId) {
		return advertService.findById(advertId);
	}

	@PutMapping
	public Advert create(@RequestBody Advert advert) {
		return advertService.create(advert);
	}

	@PatchMapping
	public Advert update(@RequestBody Advert advert) {
		return advertService.update(advert);
	}

	@DeleteMapping(value = "/{advertId}")
	public boolean deleteById(@PathVariable long advertId) {
		return advertService.delete(advertId);
	}

	@GetMapping(value = "/search/{searchString}")
	public List<Advert> search(@PathVariable String searchString) {
		return advertService.search(searchString);
	}

	@GetMapping(value = "/category/{categoryId}")
	public List<Advert> search(@PathVariable long categoryId) {
		return advertService.getAllByCategoryId(categoryId);
	}
}
