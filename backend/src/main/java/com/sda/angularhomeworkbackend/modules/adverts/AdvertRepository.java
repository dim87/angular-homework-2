package com.sda.angularhomeworkbackend.modules.adverts;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface AdvertRepository extends JpaRepository<Advert, Long> {

	List<Advert> findAllByTitleIsLikeOrDescriptionIsLike(String title, String description);

	List<Advert> findAllByCategoryId(long categoryId);
}
