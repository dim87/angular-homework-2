package com.sda.angularhomeworkbackend.modules.adverts;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class AdvertService {

	@Autowired
	private AdvertRepository advertRepository;

	@Transactional(readOnly = true)
	public List<Advert> findAll() {
		return advertRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Advert findById(final long advertId) {
		return advertRepository.findById(advertId).orElseThrow(() -> new RuntimeException(String.format("advert with ID '%s' was not found", advertId)));
	}

	@Transactional(rollbackFor = Exception.class)
	public Advert create(final Advert advert) {
		Validate.isTrue(advert.getId() == null, "id most not be defined when creating record for '%s'", advert);
		advert.setPublished(new Date());
		validateEntity(advert);
		return advertRepository.save(advert);
	}

	@Transactional(rollbackFor = Exception.class)
	public Advert update(final Advert advert) {
		Validate.notNull(advert.getId(), "id is null for '%s'", advert);
		advert.setPublished(new Date());
		validateEntity(advert);
		return advertRepository.save(advert);
	}

	private void validateEntity(final Advert advert) {
		Validate.notBlank(advert.getTitle(), "title is blank for '%s'", advert);
		Validate.notBlank(advert.getDescription(), "description is blank for '%s'", advert);
		Validate.notNull(advert.getPrice(), "title is blank for '%s'", advert);
		Validate.notNull(advert.getCategoryId(), "categoryId is blank for '%s'", advert);
		Validate.notNull(advert.getUserId(), "userId is blank for '%s'", advert);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean delete(final long advertId) {
		advertRepository.deleteById(advertId);
		return true;
	}

	@Transactional(readOnly = true)
	public List<Advert> search(final String searchString) {
		if (StringUtils.isBlank(searchString)) {
			// if empty search string was passed- skip searching
			return Collections.emptyList();
		}

		return advertRepository.findAllByTitleIsLikeOrDescriptionIsLike("%" + searchString + "%", "%" + searchString + "%");
	}

	@Transactional(readOnly = true)
	public List<Advert> getAllByCategoryId(final long categoryId) {
		return advertRepository.findAllByCategoryId(categoryId);
	}
}
