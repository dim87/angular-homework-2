package com.sda.angularhomeworkbackend.modules.categories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/categories")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@GetMapping
	public List<Category> getAll() {
		return categoryService.findAll();
	}

	@GetMapping(value = "/{categoryId}")
	public Category getById(@PathVariable long categoryId) {
		return categoryService.findById(categoryId);
	}

	@PutMapping
	public Category create(@RequestBody Category category) {
		return categoryService.create(category);
	}

	@PatchMapping
	public Category update(@RequestBody Category category) {
		return categoryService.update(category);
	}

	@DeleteMapping(value = "/{categoryId}")
	public boolean deleteById(@PathVariable long categoryId) {
		return categoryService.delete(categoryId);
	}
}
