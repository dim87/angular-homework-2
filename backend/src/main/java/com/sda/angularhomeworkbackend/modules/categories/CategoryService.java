package com.sda.angularhomeworkbackend.modules.categories;

import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Transactional(readOnly = true)
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Category findById(final long categoryId) {
		return categoryRepository.findById(categoryId).orElseThrow(
				() -> new RuntimeException(String.format("Category with ID '%s' was not found", categoryId)));
	}

	@Transactional(rollbackFor = Exception.class)
	public Category create(final Category category) {
		Validate.isTrue(category.getId() == null, "id most not be defined when creating record for '%s'", category);
		validateEntity(category);
		return categoryRepository.save(category);
	}

	@Transactional(rollbackFor = Exception.class)
	public Category update(final Category category) {
		Validate.notNull(category.getId(), "id is null for '%s'", category);
		validateEntity(category);
		return categoryRepository.save(category);
	}

	private void validateEntity(final Category category) {
		Validate.notBlank(category.getTitle(), "title is blank for '%s'", category);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean delete(final long categoryId) {
		categoryRepository.deleteById(categoryId);
		return true;
	}
}
