package com.sda.angularhomeworkbackend.modules.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping
	public List<User> getAll() {
		return userService.findAll();
	}

	@GetMapping(value = "/{userId}")
	public User getById(@PathVariable long userId) {
		return userService.findById(userId);
	}

	@PutMapping
	public User create(@RequestBody User user) {
		return userService.create(user);
	}

	@PatchMapping
	public User update(@RequestBody User user) {
		return userService.update(user);
	}

	@DeleteMapping(value = "/{userId}")
	public boolean deleteById(@PathVariable long userId) {
		return userService.delete(userId);
	}
}
