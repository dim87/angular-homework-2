package com.sda.angularhomeworkbackend.modules.users;

import java.util.List;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class UserService {

	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true)
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Transactional(readOnly = true)
	public User findById(final long userId) {
		return userRepository.findById(userId).orElseThrow(() -> new RuntimeException(String.format("User with ID '%s' was not found", userId)));
	}

	@Transactional(rollbackFor = Exception.class)
	public User create(final User user) {
		Validate.isTrue(user.getId() == null, "id most not be defined when creating record for '%s'", user);
		validateUserEntity(user);
		return userRepository.save(user);
	}

	@Transactional(rollbackFor = Exception.class)
	public User update(final User user) {
		Validate.notNull(user.getId(), "id is null for '%s'", user);
		validateUserEntity(user);
		return userRepository.save(user);
	}

	private void validateUserEntity(final User user) {
		Validate.notBlank(user.getName(), "name is blank for '%s'", user);
		Validate.notBlank(user.getSurname(), "surname is blank for '%s'", user);
		Validate.notBlank(user.getEmail(), "email is blank for '%s'", user);
	}

	@Transactional(rollbackFor = Exception.class)
	public boolean delete(final long userId) {
		userRepository.deleteById(userId);
		return true;
	}
}
