INSERT INTO users (id, name, surname, email)
VALUES (1, 'John', 'Doe', 'john.doe@gmail.com'),
       (2, 'Jane', 'Doe', 'jane.doe@gmail.com');

INSERT INTO categories (id, title)
VALUES (1, 'Cars'),
       (2, 'Notebooks'),
       (3, 'Watches');

INSERT INTO adverts (title, description, price, user_id, category_id, published)
VALUES ('Mercedes-Benz c 200 facelift avantgarde 4matic hybrid 2019my 1.5 135kw', 'Some cool description', 26000.00, 1, 1, now()),
       ('Mercedes-Benz C 220D 4Matic Amg Line 9G-Tronic Burmeister audio system', 'Another cool description', 29000.00, 2, 1, now()),
       ('BMW X6 3.0D xDrive.',
        'Pārdod BMW X6 3.0D xDrive. Jaunais 2020.gada modelis. Cena ar Pvn21%. -17% atlaide. Iespējama apdrošināšanas un līzinga noformēšana uz vietas.',
        93050.00, 1, 1, now()),
       ('BMW M550i G30 4.4i V8 462Zs X-Drive M-Sportpaket M Performance',
        'BMW M550i G30 4.4i V8 462Zs X-Drive M-Sportpaket M Performance<br>Rūpnīcas garantija līdz 11.2020 / 100000km ar iespēju pagarināt',
        29000.00, 2, 1, now()),
       ('Apple MacBook Air 13 512GB (2020)', 'MODELIS: MVH52ZE/A', 1530.00, 1, 2, now()),
       ('Apple MacBook Pro 16 Retina/TB SC i7',
        'MacBook Pro ir radīts tiem, kas alkst paveikt neiespējamo un mainīt pasauli. Jaudīgākais Apple klēpjdators ar iespaidīgu 16 collu Retina ekrānu, superātru procesoru, jaunākās paaudzes grafisko karti, MacBook Pro saimē ietilpīgāko 100 vatstundu bateriju, kas ar vienu uzlādi darbojas līdz 11 stundām, un vismaz 512GB lielu iebūvēto atmiņu ir iespaidīgs rīks īstenam profesionālim.',
        2790.00, 1, 2, now()),
       ('RADO - RADO True Thinline True Thinline',
        'The Rado True Thinline is the Swiss watch manufacturer''s thinnest timepiece. Its high-tech ceramic case is less than 5 mm thick. ', 1700.00, 1, 3,
        now()),
       ('Maurice Lacroix, MP6028-SS001-002-1, Masterpiece, Chronograph Skeleton, Ø45mm, Rokas pulkstenis',
        'Maurice Lacroix, MP6028-SS001-002-1, Masterpiece, Chronograph Skeleton, Ø45mm, Rokas pulkstenis', 4999.00, 1, 3, now());

