CREATE TABLE categories (
	id    INT AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(250) NOT NULL
);

CREATE TABLE users (
	id      INT AUTO_INCREMENT PRIMARY KEY,
	name    VARCHAR(250) NOT NULL,
	surname VARCHAR(250) NOT NULL,
	email   VARCHAR(250) NOT NULL UNIQUE
);

CREATE TABLE adverts (
	id          INT AUTO_INCREMENT PRIMARY KEY,
	title       VARCHAR(250)   NOT NULL,
	description VARCHAR(1024)  NOT NULL,
	price       DECIMAL(20, 2) NOT NULL,
	user_id     INT            NOT NULL,
	category_id INT            NOT NULL,
	published   DATETIME       NOT NULL,
	foreign key (user_id) references users (id),
	foreign key (category_id) references categories (id)
);
